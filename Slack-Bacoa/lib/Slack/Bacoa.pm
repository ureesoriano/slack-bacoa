package Slack::Bacoa;
use Dancer ':syntax';
use DateTime;
use JSON::XS;

our $VERSION = '0.2';


any [qw/get post/], '/bacoa' => sub {
    my $today = DateTime->now->day_name;

    my $yes = [
        "It is *Bacoa* o'clock.",
        "Friday? *Bacoa* time.",
        "Friday? *Bacoa* it is!",
    ];
    my $no = [
        "*Bacoa* on a $today?! Go home, you're drunk.",
        "Sorry, today is $today (that, unquestionably, means 'no')",
        "*Bacoa* on a $today..? Are you out of your mind?",
    ];
    my $msg = $today eq 'Friday'
            ? $yes->[ int(rand( scalar @$yes )) ]
            :  $no->[ int(rand( scalar @$no )) ];

    return encode_json {text => $msg};
};

true;
